import java.util.*;

class Main {

  public static void main(String[] args) {
    // Tạo danh sách học sinh
    List<Student> students = new ArrayList<>();

    Student student1 = new Student("Nguyễn Hải Nam", 22);
    students.add(student1);
    Student student2 = new Student("Nguyễn Thị Hồng", 23);
    students.add(student2);
    Student student3 = new Student("Lê Bảo Chi", 24);
    students.add(student3);
    Student student4 = new Student("Lê Đình Bảo", 23);
    students.add(student4);
    Student student5 = new Student("Lê Đình Bảo", 20);
    students.add(student5);

    System.out.println("Danh sách học sinh chưa sắp xếp");
    System.out.println(students);
    System.out.println();

    System.out.println(
        "Danh sách học sinh được sắp xếp theo quy tắc so sánh trong method compareTo ở class Student");
    Collections.sort(students);
    System.out.println(students);
    System.out.println();

    // Nếu chỉ muốn sắp xếp theo tên của sinh viên
    Comparator<Student> comparatorStudentWithFullName =
        new Comparator<Student>() {
          @Override
          public int compare(Student student, Student t1) {
            return student.getFullName().compareTo(t1.getFullName());
          }
        };
    System.out.println("Danh sách học sinh được sắp xếp theo tên");
    Collections.sort(students, comparatorStudentWithFullName);
    System.out.println(students);
    System.out.println();

    // Nếu chỉ muốn sắp xếp theo tuổi của sinh viên
    Comparator<Student> comparatorStudentWithAge =
        new Comparator<Student>() {
          @Override
          public int compare(Student student, Student t1) {
            return student.getAge() - t1.getAge();
          }
        };
    System.out.println("Danh sách học sinh được sắp xếp theo độ tuổi");
    Collections.sort(students, comparatorStudentWithAge);
    System.out.println(students);
    System.out.println();
  }
}
